# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.3.1

- patch: Update the Readme with a new Atlassian Community link.
- patch: Update the Readme with details about default variables.

## 0.3.0

- minor: Add default values for AWS variables.

## 0.2.5

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.2.4

- patch: Add warning message about new version of the pipe available.

## 0.2.3

- patch: Updated pipes toolkit version to fix coloring of log info messages.

## 0.2.2

- patch: Internal release

## 0.2.1

- patch: Updated readme with templates examples.

## 0.2.0

- minor: Add support for custom packaged template name.

## 0.1.3

- patch: Update success message with create or update.

## 0.1.2

- patch: Internal maintenance: Update Readme.

## 0.1.1

- patch: Internal maintenance: update pipes toolkit version.

## 0.1.0

- minor: Initial release.

